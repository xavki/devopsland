#!/usr/bin/bash

###############################################################
#  TITRE: 
#
#  AUTEUR:   Xavier
#  VERSION: 
#  CREATION:  
#  MODIFIE: 
#
#  DESCRIPTION: 
###############################################################



# Variables ###################################################



# Functions ###################################################

install_k0s(){
  curl -sSf https://get.k0s.sh | sudo sh
  sudo k0s install controller --single
  sudo k0s start
}

wait_api() {
    while [[ "$(curl -k --output /dev/null --silent -w ''%{http_code}'' https://127.0.0.1:6443)" != "401" ]];do
      printf '.';
      sleep 1;
    done
}

install_kubectl(){
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
  sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
  mkdir /home/vagrant/.k0s
  chown vagrant:vagrant -R /home/vagrant/.k0s
  sudo cat /var/lib/k0s/pki/admin.conf > /home/vagrant/.k0s/kubeconfig
  chown vagrant:vagrant -R /home/vagrant/.k0s
  export KUBECONFIG="/home/vagrant/.k0s/kubeconfig"
}

wait_ready(){
  kubectl wait --for='jsonpath={.status.conditions[?(@.type=="Ready")].status}=True' nodes k0s10
}


# Let's Go !! #################################################

install_k0s

wait_api

install_kubectl

wait_ready

sleep 10

kubectl get nodes
