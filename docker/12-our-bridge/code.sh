#!/usr/bin/bash

## Namespaces
NS1="x1"
NS2="x2"

## vethernet
VETH1="xeth1"
VETH2="xeth2"

## interfaces in ns
VPEER1="xpeer1"
VPEER2="xpeer2"

# ip provide for each interface
VPEER_ADDR1="10.11.0.10"
VPEER_ADDR2="10.11.0.20"

## bridge specifications
BR_ADDR="10.11.0.1"
BR_DEV="xavki0"

## namespace creation
ip netns add $NS1
ip netns add $NS2


## create vethernet and plug it to interfaces
ip link add ${VETH1} type veth peer name ${VPEER1}
ip link add ${VETH2} type veth peer name ${VPEER2}

## add interfaces in each network
ip link set ${VPEER1} netns ${NS1}
ip link set ${VPEER2} netns ${NS2}

## activate vethernet
ip link set ${VETH1} up
ip link set ${VETH2} up


ip --netns ${NS1} a
ip --netns ${NS2} a


## activate all interfaces in each ns
ip netns exec ${NS1} ip link set lo up
ip netns exec ${NS2} ip link set lo up
ip netns exec ${NS1} ip link set ${VPEER1} up
ip netns exec ${NS2} ip link set ${VPEER2} up

## define an ip for each interface
ip netns exec ${NS1} ip addr add ${VPEER_ADDR1}/16 dev ${VPEER1}
ip netns exec ${NS2} ip addr add ${VPEER_ADDR2}/16 dev ${VPEER2}

## create and activate the bridge xavki0
ip link add ${BR_DEV} type bridge
ip link set ${BR_DEV} up

## plug vethernet on the common bridge
ip link set ${VETH1} master ${BR_DEV}
ip link set ${VETH2} master ${BR_DEV}

## add an ip on the bridge 
ip addr add ${BR_ADDR}/16 dev ${BR_DEV}

## add the default route in each namespace
ip netns exec ${NS1} ip route add default via ${BR_ADDR}
ip netns exec ${NS2} ip route add default via ${BR_ADDR}

## if you want an external access
echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -t nat -A POSTROUTING -s ${BR_ADDR}/16 ! -o ${BR_DEV} -j MASQUERADE

## if you have installed docker before you need to authorize the forwarding from/to cidr
iptables -I FORWARD 1 -d 10.11.0.0/16 -s 10.11.0.0/16 -j ACCEPT
