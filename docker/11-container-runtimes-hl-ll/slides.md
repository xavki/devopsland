%title: LEARN DOCKER
%author: xavki


# 11 DOCKER : Container runtimes - High Level vs Low level


What does docker use to implement containers ?

<br>

* Docker is not the only one container runtime : lxc, crio, containerd, runC, crun, podman...

* And docker is not the main container runtime that runs your containers

* Docker uses other container runtimes behind to operate cgroups and namespaces

---------------------------------------------------------------------------------------------------

# 11 DOCKER : Container runtimes - High Level vs Low level

How is it behind the docker CLI ?

<br>

The purpose of Docker is to implement cgroups and namespaces

To do it :

Docker CLI > Docker Engine > Containerd > RunC > Namespaces/Cgroups

<br>

High Level CR > not directly implement OCI model

Docker > containerd easy to use for current user

Containerd > pull images, netwwork and volumes

RunC is a Low Level CR > implement directly the OCI

---------------------------------------------------------------------------------------------------

# 11 DOCKER : Container runtimes - High Level vs Low level

To compare to CRIO :

CRI > CRI-O > Namespaces/Cgroups


---------------------------------------------------------------------------------------------------

# 11 DOCKER : Container runtimes - High Level vs Low level

Some examples

<br>

RUNC :

```
mkdir rootfs
docker export $(docker create nginx) | tar -C rootfs -xvf -
runc run container1
runc list
runc kill container1 SIGKILL
```

---------------------------------------------------------------------------------------------------

# 11 DOCKER : Container runtimes - High Level vs Low level

Some examples

<br>

Containerd :

```
sudo ctr image pull docker.io/library/nginx:latest
sudo ctr image ls
sudo ctr run docker.io/library/nginx:latest mynginx
sudo ctr c list
sudo nsenter --target <container_pid> --net
```

---------------------------------------------------------------------------------------------------

# 11 DOCKER : Container runtimes - High Level vs Low level

Some examples

<br>

Namespaces & Cgroups

```
sudo docker inspect <container_name>
sudo lsns -p <pid>
```
